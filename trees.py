from typing import Iterable, Union, List, Set
from itertools import combinations, product
from dataclasses import astuple

from alignments import alignment, Sequences, basic_cost


class HistoryCostException(Exception):
    pass


class Node:
    """A class that represents a leaf."""
    def __init__(self, sequence: str = '', is_leaf: bool = True, left=None, right=None) -> None:
        self.sequence = sequence
        self.msa: Union[str, List[str]] = sequence
        self.is_leaf = is_leaf
        self.left = left
        self.right = right

    def __str__(self, level=0):
        return '\t'*level + '{} {}\n'.format('-', self.sequence)

    def __iter__(self):
        yield self

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, repr(self.sequence))


class InnerNode(Node):
    """A class for inner node, i.e. a node that doesnt have sequences specified by a user (not a leaf).
    It subclasses a Leaf class for convenience and for prettier type annotations."""
    def __init__(self, left: Node, right: Node, sequence: str = '') -> None:
        super().__init__(sequence, is_leaf=False, left=left, right=right)

    def __str__(self, level: int = 0) -> str:
        res = self.left.__str__(level + 1)
        res += '\t' * level + '{} {}\n'.format('<', self.sequence)
        res += self.right.__str__(level + 1)
        return res

    def __iter__(self) -> Iterable[Node]:
        """A post-order iteration, which allows for processing children before their parent."""
        yield from self.left
        yield from self.right
        yield self

    def __repr__(self) -> str:
        return '{}({}, {})'.format(self.__class__.__name__, repr(self.left), repr(self.right))


class Tree:
    """A tree class, it holds a root node and an alphabet (which comes in handy in reconstruct_ancestor method)"""
    def __init__(self, root_node: Node, alphabet: Set[str]) -> None:
        self.root_node = root_node
        self.alphabet = alphabet

    def __str__(self):
        return str(self.root_node)

    def __iter__(self) -> Iterable[Node]:
        yield from self.root_node

    def __repr__(self) -> str:
        return '{}({})'.format(self.__class__.__name__, repr(self.root_node))

    def reconstruct_ancestors(self, cost=basic_cost) -> None:
        """Reconstruct possible sequences in inner nodes based on sequences in leafs.
        Algorithm works as follows:
        For each node:
            Align MSAs from children nodes to each other
            Align sequences from children of a node
            Align above alignments to each other
            For each position in this approximate multiple sequence alignment pick a letter that is most common
            Strip it from '-' (gaps in alignment)
            Set it as a node's sequence attribute
        It works because tree traversing is post-order
        So there are always non-empty sequence attributes in descendants of a node
        Arguments:
            cost: a cost function (default: basic_cost)"""
        for node in self:
            if node.is_leaf:
                continue
            descendant_msa = alignment(Sequences(node.left.msa, node.right.msa),
                                       cost=cost)
            node.msa = descendant_msa.top + descendant_msa.bottom
            al_of_children = alignment(Sequences(node.left.sequence, node.right.sequence), cost=cost)
            seqs_to_msa = alignment(Sequences(al_of_children.top + al_of_children.bottom, node.msa), cost=cost)
            seq = []
            for i in range(len(seqs_to_msa.top[0])):
                frequencies = {}
                for letter in self.alphabet:
                    frequencies[letter] = len([seq[i] for seq in seqs_to_msa.bottom if seq[i] == letter]) \
                                          / (len(seqs_to_msa.bottom))
                opts = [seqs_to_msa.top[0][i], seqs_to_msa.top[1][i]]
                seq.append(max({opt: frequencies[opt] for opt in opts}.items(), key=lambda x: x[1])[0])
            node.sequence = ''.join(seq).replace('-', '')

    def history_cost(self, cost=basic_cost) -> int:
        """Calculate evolutionary history given a cost function"""
        hist_cost = 0
        for node in self:
            if node.is_leaf:
                continue
            if node.left.sequence == '' or node.right.sequence == '':
                raise HistoryCostException("An inner node doesn't have a sequence yet!")
            else:
                hist_cost += alignment(Sequences(node.sequence, node.left.sequence), cost=cost).cost
                hist_cost += alignment(Sequences(node.sequence, node.right.sequence), cost=cost).cost
        return hist_cost


def create_tree(sequences: List[str], cost=basic_cost) -> Tree:
    """Create a binary Tree object with sequences in leafs that minimizes evolutionary cost between nodes.
    Uses UPGMA (unweighted pair group method with arithmetic mean) method.
    Arguments:
        sequences: a list of sequences
        cost: a cost function (default: basic_cost)"""
    alphabet = set([char for sequence in sequences for char in sequence])
    alphabet.add('-')
    distances = {}
    alignments = {}
    for seq1, seq2 in combinations(sequences, 2):
        top, bot, dist = astuple(alignment(Sequences(seq1, seq2), cost=cost))
        distances[(seq1, seq2)], alignments[(seq1, seq2)] = dist, (top, bot)
        distances[(seq2, seq1)], alignments[(seq2, seq1)] = distances[(seq1, seq2)], alignments[(seq1, seq2)]
    closest_sequences = tuple(min(distances.items(), key=lambda x: x[1])[0])
    sequences = list(map(lambda x: Node(x), [x for x in sequences if x not in closest_sequences]))
    sequences.append(InnerNode(Node(closest_sequences[0]), Node(closest_sequences[1])))
    while len(sequences) > 1:
        current_distances = []
        for combs in combinations(sequences, 2):
            s, n = 0, 0
            for node1, node2 in product(combs[0], combs[1]):
                if node1.sequence != '' and node2.sequence != '':
                    s += distances[(node1.sequence, node2.sequence)]
                    n += 1
            current_distances.append((combs, s / n))
        closest_nodes = tuple(min(current_distances, key=lambda x: x[1])[0])
        for node in closest_nodes:
            sequences.remove(node)
        sequences.append(InnerNode(closest_nodes[0], closest_nodes[1]))
    return Tree(sequences[0], alphabet)



