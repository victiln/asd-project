from itertools import product
from typing import Union, List
from dataclasses import dataclass

import numpy as np


def basic_cost(sym1: str, sym2: str) -> int:
    """Return a cost for two symbols.
    1 if they are not equal, 0 if they are.
    """
    if sym1 == sym2:
        return 0
    else:
        return 1


@dataclass
class Alignment:
    """Class for storing alignments"""
    top: List[str]
    bottom: List[str]
    cost: int = 0


@dataclass
class Sequences:
    """Class for fetching sequences"""
    sequences1: Union[str, List[str]]
    sequences2: Union[str, List[str]]


def alignment(sequences: Sequences, cost=basic_cost) -> Alignment:
    """Perform a Needleman-Wunsch global sequence alignments.
    In case there is more than one sequence in one of lists of sequences a function performs an approximate MSA.
    An approximate multiple sequence alignment works like a usual alignment
    But it averages over a costs in a given position.
    Arguments:
        sequences: a Sequences object of sequences to be aligned
        cost: a cost function (default: basic_cost)
    """
    sequences1, sequences2 = sequences.sequences1, sequences.sequences2
    if isinstance(sequences1, str):
        sequences1 = [sequences1]
    if isinstance(sequences2, str):
        sequences2 = [sequences2]
    scoring_table = np.zeros((len(sequences1[0]) + 1, len(sequences2[0]) + 1), dtype=int)
    arrows_table = np.empty((len(sequences1[0]) + 1, len(sequences2[0]) + 1), dtype='<U10')
    arrows_table[0, :], arrows_table[:, 0] = 'left', 'top'
    for j in range(1, len(sequences2[0]) + 1):
        scoring_table[0, j] = scoring_table[0, j - 1] + sum([cost('-', sequence[j - 1])
                                                            for sequence in sequences2]) / len(sequences2)
    for i in range(1, len(sequences1[0]) + 1):
        scoring_table[i, 0] = scoring_table[i - 1, 0] + sum([cost(sequence[i - 1], '-')
                                                             for sequence in sequences1]) / len(sequences1)
    for i, j in product(range(1, len(sequences1[0]) + 1), range(1, len(sequences2[0]) + 1)):
        seqs1_to_seqs2 = sum([cost(sequence1[i - 1], sequence2[j - 1])
                              for sequence1, sequence2 in product(sequences1, sequences2)]) / len(sequences1)
        seqs1_to_gap = sum([cost(sequence[i - 1], '-') for sequence in sequences1]) / len(sequences1)
        seqs2_to_gap = sum([cost('-', sequence[j - 1]) for sequence in sequences2]) / len(sequences2)
        arrows_table[i, j], scoring_table[i, j] = min(
            ('diagonal', scoring_table[i - 1, j - 1] + seqs1_to_seqs2),
            ('top', scoring_table[i - 1, j] + seqs1_to_gap),
            ('left', scoring_table[i, j - 1] + seqs2_to_gap),
            key=lambda x: x[1]
        )
    top, bottom = [[] for _ in range(len(sequences1))], [[] for _ in range(len(sequences2))]
    i, j = len(sequences1[0]), len(sequences2[0])
    while i > 0 or j > 0:
        if arrows_table[i, j] == 'diagonal':
            for k in range(len(top)):
                top[k].append(sequences1[k][i - 1])
            for k in range(len(bottom)):
                bottom[k].append(sequences2[k][j - 1])
            i -= 1
            j -= 1
        elif arrows_table[i, j] == 'left':
            for k in range(len(top)):
                top[k].append('-')
            for k in range(len(bottom)):
                bottom[k].append(sequences2[k][j - 1])
            j -= 1
        else:
            for k in range(len(top)):
                top[k].append(sequences1[k][i - 1])
            for k in range(len(bottom)):
                bottom[k].append('-')
            i -= 1
    top = list(map(lambda q: ''.join(reversed(q)), top))
    bottom = list(map(lambda q: ''.join(reversed(q)), bottom))
    return Alignment(top, bottom, scoring_table[len(sequences1[0]), len(sequences2[0])])

