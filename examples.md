

Some examples:


```python
from trees import create_tree
```


```python
seqs = 'ATAAGGCCAT ACGTTAAGCGT ATAAAGCCAT ACGTAAGCTT ACGTAAAGCGT'.split()
```

A simple tree with given sequences as leafs, an str dunder method makes it readable


```python
t = create_tree(seqs)
print(t)
```

    		- ATAAGGCCAT
    	< 
    		- ATAAAGCCAT
    < 
    		- ACGTAAGCTT
    	< 
    			- ACGTTAAGCGT
    		< 
    			- ACGTAAAGCGT
    


<p>Now we reconstruct ancestral history and print a history cost
<p>Given out simple cost function it's basically edit distance (Levenshtein distance)</p>


```python
t.reconstruct_ancestors()
print(t)
print("History cost: {}".format(t.history_cost()))
```

    		- ATAAGGCCAT
    	< ATAAGGCCAT
    		- ATAAAGCCAT
    < ATAAGCAT
    		- ACGTAAGCTT
    	< ACGTAAGCGT
    			- ACGTTAAGCGT
    		< ACGTTAAGCGT
    			- ACGTAAAGCGT
    
    History cost: 9


<p>A more fun example: I'd like to know a name of the LCA of currently living monkeys and homo sapiens</p>


```python
species = ['Homo sapines', 'Pan troglodytes', 'Pan paniscus',
           'Gorilla beringei', 'Gorilla gorilla', 'Pongo pygmaeus', 
           'Pongo abelli', 'Pongo tapanuliensis', 'Symphalangus syndactylus',
           'Hylobates lar', 'Papio ursinus']
```

Ok, that's enough. Now let's find out what was a taxonomic name of out LCA.


```python
monkeys_tree = create_tree(species)
print(monkeys_tree)
```

    	- Symphalangus syndactylus
    < 
    			- Gorilla beringei
    		< 
    			- Gorilla gorilla
    	< 
    			- Hylobates lar
    		< 
    				- Pan troglodytes
    			< 
    					- Pongo tapanuliensis
    				< 
    						- Pongo abelli
    					< 
    								- Homo sapines
    							< 
    								- Papio ursinus
    						< 
    								- Pan paniscus
    							< 
    								- Pongo pygmaeus
    


Sadly, it didn't even recognize how close in evolutionary history common chimpanzees (Pan troglodytes) and Homo sapiens are.
Maybe generated names will be interesting.


```python
monkeys_tree.reconstruct_ancestors()
print(monkeys_tree)
```

    	- Symphalangus syndactylus
    < ls tylus
    			- Gorilla beringei
    		< Gorilla berigei
    			- Gorilla gorilla
    	< ll bigei
    			- Hylobates lar
    		< ylobates
    				- Pan troglodytes
    			< ogodytes
    					- Pongo tapanuliensis
    				< ogo aiensi
    						- Pongo abelli
    					< ogo abelli
    								- Homo sapines
    							< Homo sapines
    								- Papio ursinus
    						< omo sapinus
    								- Pan paniscus
    							< Pan paniscus
    								- Pongo pygmaeus
    


"ls tylus". Now we know. 


