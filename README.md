# Trees

It was a final project for Algorithms and Data Structures class I attended to. 
Since I study Bioinformatics it was mostly a test if students understand a Needleman-Wunsch alignment algorithm.
It also showed us how one can implement (phylogenetic) trees in Python.
The goal was to 
* Generate a phylogenetic tree with sequences in leafs, constructed in a way that would minimize evolutionary cost of a whole tree
(so maximum parsimony method, which is actually outdated).
* Generate possible ancestral sequences that minimize evolutionary cost of a whole tree.
<p>For generating a tree topology I used UPMGA method, and for generating ancestral sequences I used approximate multiple sequence alignments. </p>
<p>I finished the project a year ago, now I thought I could use it to test some Python features.
So there are type declarations and some use of dataclasses (although the Node class could be a dataclass itself).
The logic behind generating trees is the same.</p>
<p>There are some examples in a notebook [examples.ipynb](examples.ipynb) and in a markdown version [examples.md](examples.md).</p>
<p>This project requires numpy.</p>

# TODO
* Change Node (and InnerNode) class to be a dataclass
* Maybe implement other topology construction methods (such as maximum likelihood)

# License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
